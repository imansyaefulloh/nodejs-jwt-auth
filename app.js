const express = require('express');
const jwt = require('jsonwebtoken');

const app = express();

/**
 * Format Token
 * Authorization: Bearer {token}
 */
function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    req.token = bearerToken;

    next();
  } else {
    console.log('error 1');
    res.sendStatus(403);
  }
}

app.get('/api', (req, res) => {
  res.json({
    message: 'Welcome to API'
  });
});

app.post('/api/posts', verifyToken, (req, res) => {
  console.log(req.token);
  jwt.verify(req.token, 'secret123', (err, authData) => {
    if (err) {
      console.log('error 2');
      res.sendStatus(403);
    } else {
      res.json({
        message: 'Post created',
        authData
      });
    }
  });
});

app.post('/api/login', (req, res) => {
  // Mock User
  const user = {
    id: 1,
    username: 'iman',
    email: 'iman@gmail.com'
  };

  jwt.sign({user}, 'secret123', (err, token) => {
    res.json({token});
  });
});

app.listen(3000, () => {
  console.log('server running at http://localhost:3000');
})